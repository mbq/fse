library(praznik)
library(Boruta)

data(srx)


hScores(srx)|>print()

miScores(srx,srx$Y)|>print()

cmiScores(srx,srx$Y,srx$A)|>print()

plot(miScores(srx,srx$Y),cmiScores(srx,srx$Y,srx$A))

triScores(srx)->ti; ti[order(ti$MI),]|>print()



